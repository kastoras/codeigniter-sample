/*2.Query which fetch 10 first post of a blog. Where slug is blogs slug*/
SELECT * FROM `posts` p 
INNER JOIN users u on p.blog_id = u.blog_user_id
WHERE u.blog_url=slug ORDER BY p.`created_at` DESC LIMIT 10

/*Working example*/
SELECT * FROM `posts` p 
INNER JOIN users u on p.blog_id = u.blog_user_id
WHERE u.blog_url='nikos' ORDER BY p.`created_at` DESC LIMIT 10





/*3.Query which fetch post categories. Where id is post id*/
SELECT DISTINCT c.name FROM `posts` p 
INNER JOIN posts_to_categories ptc ON p.id = ptc.post_id
INNER JOIN categories c ON ptc.category_id = c.id
WHERE p.id = id

/*Working example*/
SELECT DISTINCT c.name FROM `posts` p 
INNER JOIN posts_to_categories ptc ON p.id = ptc.post_id
INNER JOIN categories c ON ptc.category_id = c.id
WHERE p.id = 1




/*4.Query which fetch most relevant post for post given. Xhere id is post id*/
SELECT posts.title, 
       alltable.simmilar_categories 
FROM   (SELECT ptc2.post_id        AS post_id_inner, 
               Count(ptc2.post_id) AS simmilar_categories 
        FROM   posts_to_categories ptc2 
        WHERE  ptc2.category_id IN (SELECT DISTINCT ptc.category_id 
                                    FROM   `posts_to_categories` ptc 
                                    WHERE  ptc.post_id = id) 
        GROUP  BY ptc2.post_id) alltable 
       INNER JOIN posts 
               ON posts.id = post_id_inner 
WHERE  alltable.simmilar_categories = (SELECT DISTINCT Max( 
       max_simmilar_table.simmilar_categories) AS 
                        max_categories_simmilar 
                                       FROM 
              (SELECT 
              ptc2.post_id        AS post_id_inner, 
              Count(ptc2.post_id) AS simmilar_categories 
                                               FROM   posts_to_categories ptc2 
                                               WHERE 
              ptc2.category_id IN (SELECT DISTINCT ptc.category_id 
                                   FROM   `posts_to_categories` ptc 
                                   WHERE  ptc.post_id = id) 
                                               GROUP  BY ptc2.post_id) 
              max_simmilar_table 
                                             ) 
       AND post_id_inner <> id

/*Working example*/
SELECT posts.title, 
       alltable.simmilar_categories 
FROM   (SELECT ptc2.post_id        AS post_id_inner, 
               Count(ptc2.post_id) AS simmilar_categories 
        FROM   posts_to_categories ptc2 
        WHERE  ptc2.category_id IN (SELECT DISTINCT ptc.category_id 
                                    FROM   `posts_to_categories` ptc 
                                    WHERE  ptc.post_id = 3) 
        GROUP  BY ptc2.post_id) alltable 
       INNER JOIN posts 
               ON posts.id = post_id_inner 
WHERE  alltable.simmilar_categories = (SELECT DISTINCT Max( 
       max_simmilar_table.simmilar_categories) AS 
                        max_categories_simmilar 
                                       FROM 
              (SELECT 
              ptc2.post_id        AS post_id_inner, 
              Count(ptc2.post_id) AS simmilar_categories 
                                               FROM   posts_to_categories ptc2 
                                               WHERE 
              ptc2.category_id IN (SELECT DISTINCT ptc.category_id 
                                   FROM   `posts_to_categories` ptc 
                                   WHERE  ptc.post_id = 3) 
                                               GROUP  BY ptc2.post_id) 
              max_simmilar_table 
                                             ) 
       AND post_id_inner <> 3