-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Φιλοξενητής: 127.0.0.1
-- Χρόνος δημιουργίας: 28 Μάη 2019 στις 16:07:00
-- Έκδοση διακομιστή: 10.1.32-MariaDB
-- Έκδοση PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Βάση δεδομένων: `codeigniter`
--

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `blog_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Άδειασμα δεδομένων του πίνακα `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `blog_id`) VALUES
(2, 'Football', 'footbal', 1),
(3, 'Basket', 'basket', 1),
(7, 'Training', 'training', 1),
(8, 'Gym', 'gym', 1);

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Άδειασμα δεδομένων του πίνακα `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('1uispqg92t397e8bijmdkddmilrh7hf5', '::1', 1559044001, 0x5f5f63695f6c6173745f726567656e65726174657c693a313535393034343030313b),
('gc5u71qfj2t2qlnvt89iippdo4nvqthl', '::1', 1559043663, 0x5f5f63695f6c6173745f726567656e65726174657c693a313535393034333636333b),
('hhpb0rrklog584co0f9kslsa9ue8baq6', '::1', 1559043253, 0x5f5f63695f6c6173745f726567656e65726174657c693a313535393034333235333b),
('isnenctg1sideictpu6hntg2afg4iq0o', '::1', 1559044015, 0x5f5f63695f6c6173745f726567656e65726174657c693a313535393034343030313b);

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `blog_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `content` text,
  `slug` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Άδειασμα δεδομένων του πίνακα `posts`
--

INSERT INTO `posts` (`id`, `blog_id`, `title`, `content`, `slug`, `created_at`) VALUES
(1, 1, 'Paok Chamption A', 'Lorem ipsum', 'paok-chamption-a', '2019-05-24 17:06:25'),
(2, 1, 'New Team 3', 'Lorem Ipsum', 'new-team-3', '2019-05-25 01:27:34'),
(3, 1, 'Training goals', 'Lorem Ipsum', 'training-goals', '2019-05-25 01:28:32'),
(4, 1, 'Weights', 'Lorem Ipsum', 'weights', '2019-05-25 01:31:01');

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `posts_to_categories`
--

CREATE TABLE `posts_to_categories` (
  `post_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Άδειασμα δεδομένων του πίνακα `posts_to_categories`
--

INSERT INTO `posts_to_categories` (`post_id`, `category_id`) VALUES
(1, 2),
(1, 3),
(1, 8),
(2, 2),
(2, 3),
(2, 8),
(3, 2),
(3, 3),
(4, 2),
(4, 8);

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `users`
--

CREATE TABLE `users` (
  `blog_user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `blog_url` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Άδειασμα δεδομένων του πίνακα `users`
--

INSERT INTO `users` (`blog_user_id`, `email`, `name`, `password`, `blog_url`, `created_at`) VALUES
(1, 'test@test.gr', 'Nikos', '$2y$10$.PU3KUAqZCzuKnLQidJgfO5DQfbu2kplRxdWLWSuqk5Epxg.6tije', 'nikos', '2019-05-24 15:59:09'),
(2, 'nikos@antimidis.gr', 'Nik', '$2y$10$.PU3KUAqZCzuKnLQidJgfO5DQfbu2kplRxdWLWSuqk5Epxg.6tije', 'nik', '0000-00-00 00:00:00'),
(3, 'petros@papadopoulos.gr', 'Peter', '087408522c31eeb1f982bc0eaf81d35f', '510', '0000-00-00 00:00:00'),
(4, 'ada@tagr', 'Ada', '087408522c31eeb1f982bc0eaf81d35f', '510', '2019-05-24 20:43:48');

--
-- Ευρετήρια για άχρηστους πίνακες
--

--
-- Ευρετήρια για πίνακα `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uq_categories` (`name`,`blog_id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `blog_id` (`blog_id`);

--
-- Ευρετήρια για πίνακα `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Ευρετήρια για πίνακα `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `blog_id` (`blog_id`);

--
-- Ευρετήρια για πίνακα `posts_to_categories`
--
ALTER TABLE `posts_to_categories`
  ADD PRIMARY KEY (`post_id`,`category_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `posts_to_categories_idx_id_id` (`category_id`,`post_id`);

--
-- Ευρετήρια για πίνακα `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`blog_user_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT για άχρηστους πίνακες
--

--
-- AUTO_INCREMENT για πίνακα `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT για πίνακα `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT για πίνακα `users`
--
ALTER TABLE `users`
  MODIFY `blog_user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Περιορισμοί για άχρηστους πίνακες
--

--
-- Περιορισμοί για πίνακα `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`blog_id`) REFERENCES `users` (`blog_user_id`);

--
-- Περιορισμοί για πίνακα `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`blog_id`) REFERENCES `users` (`blog_user_id`);

--
-- Περιορισμοί για πίνακα `posts_to_categories`
--
ALTER TABLE `posts_to_categories`
  ADD CONSTRAINT `posts_to_categories_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  ADD CONSTRAINT `posts_to_categories_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
