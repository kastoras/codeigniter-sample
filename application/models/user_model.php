<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model{

	/**
	 * User/Blog id
	 * @var int
	 */
	private $blog_user_id;
	
	/**
	 * User email
	 * @var string
	 */
	private $email;
	
	/**
	 * User name
	 * @var string
	 */
	private $name;

	/**
	 * User Passw
	 * @var string
	 */
	private $password;

	/**
	 * Slug for users blog url
	 * @var string
	 */
	private $blog_url;

	/**
	 * User created timestamp
	 * @var string
	 */
	private $created_at;

	/**
	 * if initialize model with id load current model
	 * 
	 * @param int $id Uset id
	 */
	function __construct($id=null){
		parent::__construct();
		$this->load->helper('url');
		if($id){
			$user = $this->db->get_where('users', array('blog_user_id' => $id));

			$this->blog_user_id  = $user->row()->blog_user_id;
			$this->email  = $user->row()->email;
			$this->name  = $user->row()->name;
			$this->password  = $user->row()->password;
			$this->blog_url  = $user->row()->blog_url;
			$this->created_at  = $user->row()->created_at;
		}
	}


	/**
	 * Inserts new user with given email, password, name
	 * @param  string $email     User email
	 * @param  string $password  User selected password
	 * @param  string $name      User name
	 * @return int           	 User id
	 */
    public function insert($email,$password,$name){

		$data = [ 
			'email' => $email,
			'name' => $name,
			'password' => md5($password),
			'blog_url' => url_title($name, 'dash', TRUE)
		];

        $this->db->insert('users', $data);

        return $this->db->insert_id();
    }

    /**
     * Get user data if initialized
     * @return object User_model
     */
	public function get(){
		if($this->blog_user_id){
			return $this;	
		}
		return 'no User!';
		
	}

}