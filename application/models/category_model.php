<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model{

	/**
	 * To difine which user/blog is active
	 * @var int
	 */
	private $blog_id;

	function __construct($blog_id=null){
		parent::__construct();
        $this->load->helper('url');
		$this->blog_id = $blog_id;
	}

    /**
     * Insert new category for current post/user
     * @param  string $name Category name
     * @return boolean      
     */
    public function insert($name){

    	$data = [
    		'name'=>$name,
    		'blog_id'=>$this->blog_id,
    		'slug'=>url_title($name, 'dash', TRUE)
    	];

        return $this->db->insert('categories', $data);
    }

    /**
     * Update given category with given value
     * @param  int 		$id 	Category id  
     * @param  string 	$name
     * @return boolean  
     */
    public function update($id,$name){

    	$data = [
    		'name'=>'',
    		'slug'=>url_title($name, 'dash', TRUE)
    	];

        return $this->db->update('categories', $data, ['id' => $id]);
    }    

    /**
     * Return all categories for currnt blog/user
     * @return Array
     */
    public function categories_all(){

    	return $this->db->get_where('categories', ['blog_id =' => $this->blog_id])->result();
    }

}