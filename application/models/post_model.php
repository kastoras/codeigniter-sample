<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_model extends CI_Model{

	/**
	 * To difine which user/blog is active
	 * @var int
	 */
	private $blog_id;

	function __construct($blog_id=null){
		parent::__construct();
		$this->load->helper('url');
		$this->blog_id = $blog_id;
	}

	/**
	 * Insert new post
	 * @param  string $title   	post title
	 * @param  string $content 	post content
	 * @return int             	id of the inserted post
	 */
    public function insert($title,$content){

    	$data=[
	    	'title' 	=> $title,
			'blog_id' 	=> $this->blog_id,		
			'content' 	=> $content,		
			'slug' 		=> url_title($title, 'dash', TRUE)
    	];
		
        $this->db->insert('posts', $data);
        $this->id = $this->db->insert_id();        
    
        return $this->id;        
    }

    /**
     * Insert given post to given category
     * @param  int 		$post_id     
     * @param  int 		$category_id 
     * @return boolean           
     */
    public function post_to_category($post_id, $category_id){

    	$data = [
    		'post_id' 		=> $post_id,
    		'category_id' 	=> $category_id 
    	];

    	return $this->db->insert('posts_to_categories', $data);
    }

    /**
     * Change post categories, deletes old then add given
     * @param  int      $id         Post id
     * @param  array    $categories Categories to add
     * @return void
     */
    public function save_post_categories($id,$categories){
        $this->db->delete('posts_to_categories',['post_id'=>$id]);
        foreach ($categories as $category) {
            $this->post_to_category($id,$category);
        }
    }

    /**
     * Update given post id with given values
     * @param  int 		$id      
     * @param  string 	$title   
     * @param  string 	$content 
     * @return boolean         
     */
    public function update($id,$title,$content){

    	$data = [
	    	'title' 	=> $title,	
			'content' 	=> $content,		
			'slug' 		=> url_title($title, 'dash', TRUE)
    	];

        return $this->db->update('posts', $data, ['id' => $id]);
    }

    /**
     * Return post data for given id
     * @param  int 		$id 	post id
     * @return array 			array of post data
     */
    public function get_single($id){

    	return $this->db->get_where('posts', ['id =' => $id])->result()[0];
    }

    /**
     * Return all posts for current blog.user
     * @return array 		Array of posts data 
     */	
    public function get_all(){

    	return $this->db->get_where('posts', ['blog_id =' => $this->blog_id])->result();
    }

    /**
     * Return all post for current blog/user for category given
     * @param  int 		$category_id 
     * @return array              		Array of posts
     */
    public function get_category($category_id){

    	return $this->db->select('p.*')
    		->from('posts p')
    		->join('posts_to_categories ptc', 'p.id = ptc.post_id')
    		->where('ptc.category_id', $category)
    		->get()->result();
    }

    /**
     * Return first ten posts of current user/blog
     * @return Array
     */
    public function get_first_ten_posts(){
    	return $this->db->select('p.*')
    		->from('posts p')
    		->join('users u', 'p.blog_id = u.blog_user_id')
    		->where('u.blog_user_id', $this->blog_id)
    		->order_by('p.created_at', 'DESC')
    		->limit(10)
    		->get()->result();
    }

    /**
     * Return categories of given Post
     * @param  int 		$post_id
     * @return array
     */
    public function post_categories($post_id){
        return $this->db->select('c.name')
            ->distinct()
            ->from('posts p')
            ->join('posts_to_categories ptc', 'p.id = ptc.post_id')
            ->join('categories c', 'ptc.category_id = c.id')
            ->where('p.id', $post_id)
            ->get()->result();
    }    

    /**
     * Return most relevant post and number of similar categories with post given
     * @param  int      $post_id
     * @return array               
     */
    public function get_simmilar_posts($post_id){
        return $this->db->select('posts.title, alltable.simmilar_categories')
                ->from('(SELECT ptc2.post_id        AS post_id_inner, 
               Count(ptc2.post_id) AS simmilar_categories 
        FROM   posts_to_categories ptc2 
        WHERE  ptc2.category_id IN (SELECT DISTINCT ptc.category_id 
                                    FROM   `posts_to_categories` ptc 
                                    WHERE  ptc.post_id = '.$post_id.') 
        GROUP  BY ptc2.post_id) alltable 
       INNER JOIN posts 
               ON posts.id = post_id_inner')
                ->where(' alltable.simmilar_categories = (SELECT DISTINCT Max( 
       max_simmilar_table.simmilar_categories) AS 
                        max_categories_simmilar 
                                       FROM 
              (SELECT 
              ptc2.post_id        AS post_id_inner, 
              Count(ptc2.post_id) AS simmilar_categories 
                                               FROM   posts_to_categories ptc2 
                                               WHERE 
              ptc2.category_id IN (SELECT DISTINCT ptc.category_id 
                                   FROM   `posts_to_categories` ptc 
                                   WHERE  ptc.post_id = '.$post_id.') 
                                               GROUP  BY ptc2.post_id) 
              max_simmilar_table 
                                             ) 
       AND post_id_inner <> '.$post_id, NULL, FALSE)->get()->result();

   
    }
}