<?php $this->load->view('blocks/header'); ?>
        <div id="content-wrapper">

            <div class="container-fluid">

                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Post Edit</li>
                </ol>

                <!-- DataTables Example -->
                <div class="mb-3">
                    <form action="/index.php/post/edit/<?php echo $id; ?>" method="post">
                        <div class="form-group">
                            <label for="title">Title:</label>
                            <input type="text" class="form-control" id="title" name="title" value="<?php echo $title; ?>">
                        </div>
                        <div class="form-group">
                            <label for="content">Content:</label>
                            <textarea class="form-control" id="content" name="content"><?php echo $content; ?></textarea>
                        </div>
                        <div class="checkbox">
                            <label>Categories</label><br>
                            <?php foreach($categories as $category){ ?>
                            <label>
                                <input type="checkbox" name="categories[]" <?php echo isset($category['selected']) ? 'checked' : ''; ?> value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?>
                           </label>
                            <?php } ?>
                        </div>
                        <button type="submit" class="btn btn-success">Submit</button>
                    </form>
                </div>                
            </div>
        </div>

            <!-- /.container-fluid -->
<?php $this->load->view('blocks/footer'); ?>