<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends CI_Controller {

    function __construct(){
        parent::__construct();
        
    }

    /**
     * Handle user authentication - authorization. First search for authenticated user Session. 
     * If there is not, tries to authenticate with useremai & password
     * Passwords should be stored with php password_hash method.
     * @param  string   $useremail 
     * @param  string   $password  
     * @return boolean              True if authorized user, false if not
     */
    public function authenticate($useremail=null,$password=null){
        $this->load->library('session');
        if($this->session->registered_user){
            session_regenerate_id();
            return true;
        }
        else{
            if(!empty($user_data = $this->username_exists($useremail))){
                
                if (password_verify($password, $user_data['hased_password'])) {
                    $this->session->set_userdata('registered_user', $user_data['user_name']);
                    $this->session->set_userdata('blog_id', $user_data['blog_id']);
                    return true;
                } else {
                    return false;
                }
            }
            return false;
        }
    }

    /**
     * Check if user exists or not
     * @param  string           $useremail
     * @return boolean|array                If user exists return array with username and hassed password else return false
     */
    private function username_exists($useremail){
        $query = $this->db->select('password,name,blog_user_id')
            ->from('users')
            ->where('users.email', $useremail)
            ->get()->result();
            
        if(!empty($query)){
            return [
                'user_name' => $query[0]->name,
                'hased_password' => $query[0]->password,
                'blog_id' => $query[0]->blog_user_id,
            ];
        } else {
            return false;
        }
    }
}