<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends CI_Controller {

    /**
     * Var to initialize post model
     * @var Object
     */
    public $post_model;

    /**
     * Var to initialize post category model
     * @var Object
     */
    public $post_categories;

    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('Authentication');
        $this->load->model('post_model');
        $this->load->model('category_model');        
        $this->load->library('session');

        $this->post_model = new Post_model($this->session->blog_id);
        $this->all_cat_model = new Category_model($this->session->blog_id);

        /**
         * Check if user is loged in
         */
        if(!$this->session->registered_user){
             redirect('', 'refresh');
        }
        else{
            session_regenerate_id();
        } 
    }

    /**
     * @TODO show single post, frontend
     */
    public function single($id){

    }

    /**
     * Edit/show single post on admin panel
     * @param  int      $id     post id
     * @return show edit page
     */
    public function edit($id){

        /**
         * If there is a post request, store data and edirect to post list
         */
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $this->post_model->update($id,$this->input->post('title'),$this->input->post('content'));
            $this->post_model->save_post_categories($id,$this->input->post('categories'));
            redirect('blog/dashboard', 'refresh');
        }

        $categories = $categories_selected = [];  

        $post = $this->post_model->get_single($id);   
        $post_categories = $this->post_model->post_categories($id);

        $all_categories = $this->all_cat_model->categories_all();

        foreach ($post_categories as $category) {
            $categories[] = $category->name; 
        }

        foreach ($all_categories as $value) {
            if(in_array($value->name, $categories)){
                $categories_selected[]=[
                    'name'=>$value->name,
                    'id'=>$value->id,
                    'selected'=>1
                ];
            }
            else {
                $categories_selected[]=[
                    'name'=>$value->name,
                    'id'=>$value->id
                ];
            }
        }

        $data = [
            'id'=>$post->id,
            'blog_id'=>$post->blog_id,
            'title'=>$post->title,
            'content'=>$post->content,
            'categories'=>$categories_selected
        ];

        $this->load->view('edit_single_post', $data);
    }

}