<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
    }

    /**
     * Action to show user dashboard
     * @return user dashboard page
     */
    public function dashboard(){
        $this->load->library('session');

        /**
         * Check if user is loged in
         */        
        if(!$this->session->registered_user){
             redirect('', 'refresh');
        }
        else{
            session_regenerate_id();
        } 
        
        $this->load->model('category_model');
        $this->load->model('post_model');   

        $categories_model = new Category_model($this->session->blog_id);
        $data['categories'] = $categories_model->categories_all();


        $post_model = new Post_model($this->session->blog_id);
        $data['posts'] = $post_model->get_all();        

        $this->load->view('dashboard',$data);

    }

    /**
     * Method for user login
     * @return Redirect to dashboard page if success / login for if not success
     */
    public function login(){
        $this->load->helper('Authentication');
        $auth_obj=new Authentication();

        if($this->input->post('email') && $this->input->post('password')){
            
            if($auth_obj->authenticate($this->input->post('email'),$this->input->post('password'))){
                redirect('blog/dashboard', 'refresh');
            }
            
        }
        $this->load->view('login');
    }

    /**
     * Method for loging off
     * @return redirect to home page
     */
    public function logout(){
        $this->load->library('session');
        $this->session->sess_destroy();
        redirect('', 'refresh');
    }

}